﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR.iOS;

public class ARSceneManager : MonoBehaviour {

    // Public member variables
    public Transform m_targetTransform;
    public Transform m_machineParentTransform;
    public GameObject[] m_machines;

    // Private member variables
    CaptureAndSave snapShot;

    private bool isDetected = false;
    private GameObject selectedObj = null;

    // Use this for initialization
	void Start () {
        snapShot = GameObject.FindObjectOfType<CaptureAndSave>();

        isDetected = false;
        m_targetTransform.gameObject.SetActive(true);
	}
	
    void OnEnable()
    {
        CaptureAndSaveEventListener.onError += OnError;
        CaptureAndSaveEventListener.onSuccess += OnSuccess;
    }

    void OnDisable()
    {
        CaptureAndSaveEventListener.onError -= OnError;
        CaptureAndSaveEventListener.onSuccess -= OnSuccess;
    }

    void OnError(string error)
    {
        Debug.Log("Error : " + error);
    }

    void OnSuccess(string msg)
    {
        Debug.Log("Success : " + msg);
    }

    // Update is called once per frame
	void Update () {
        SetTarget();
        SelectKiosk();
        RotateAndScaleKiosk();
	}

    // Public Helper methods
    public void SetTarget()
    {
        Vector3 screenPosition = Camera.main.ScreenToViewportPoint(new Vector3(Screen.width / 2, Screen.height / 2, 0));
        ARPoint point = new ARPoint
        {
            x = screenPosition.x,
            y = screenPosition.y
        };

        // prioritize reults types
        ARHitTestResultType[] resultTypes = {
                        ARHitTestResultType.ARHitTestResultTypeExistingPlaneUsingExtent, 
                        // if you want to use infinite planes use this:
                        ARHitTestResultType.ARHitTestResultTypeExistingPlane,
                        ARHitTestResultType.ARHitTestResultTypeHorizontalPlane,
                        ARHitTestResultType.ARHitTestResultTypeFeaturePoint
                    };

        foreach (ARHitTestResultType resultType in resultTypes)
        {
            if (HitTestWithResultType(point, resultType))
            {
                isDetected = true;
                m_targetTransform.gameObject.SetActive(true);
                return;
            }
        }
    }

    bool HitTestWithResultType(ARPoint point, ARHitTestResultType resultTypes)
    {
        List<ARHitTestResult> hitResults = UnityARSessionNativeInterface.GetARSessionNativeInterface().HitTest(point, resultTypes);
        if (hitResults.Count > 0)
        {
            foreach (var hitResult in hitResults)
            {
                //Debug.Log("Got hit!");
                m_targetTransform.position = UnityARMatrixOps.GetPosition(hitResult.worldTransform);
                //m_HitTransform.rotation = UnityARMatrixOps.GetRotation(hitResult.worldTransform);
                //Debug.Log(string.Format("x:{0:0.######} y:{1:0.######} z:{2:0.######}", m_HitTransform.position.x, m_HitTransform.position.y, m_HitTransform.position.z));
                return true;
            }
        }
        return false;
    }

    public void SelectKiosk()
    {
        if(Input.GetMouseButtonDown(0))
        {
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;
            if(Physics.Raycast(ray, out hit))
            {
                if(hit.collider.tag == "Kiosk")
                {
                    KioskItem item = hit.collider.transform.GetComponentInParent<KioskItem>();
                    if(selectedObj != null)
                        selectedObj.GetComponent<KioskItem>().SelectKiosk(false);
                    selectedObj = item.gameObject;
                    selectedObj.GetComponent<KioskItem>().SelectKiosk(true);
                }
            }
        }
    }

    private int prevTouchCount = 0;
    private bool isRotate = false, isScale = false;
    private Vector2 prevPos;
    private Vector3 prevScale;
    private float prevDist;

    public void RotateAndScaleKiosk()
    {
        if (selectedObj == null)
            return;
        
        //------Rotate------//
        if (Input.touchCount == 1)
        {
            var touch = Input.GetTouch(0);
            if (prevTouchCount == 0)
            {
                isRotate = true;
                prevPos = touch.position;
            }
            if (isRotate)
            {
                float dx = touch.position.x - prevPos.x;
                float dy = touch.position.y - prevPos.y;
                selectedObj.transform.Rotate(0, -dy, 0);
                prevPos = touch.position;
            }
        }
        else
            isRotate = false;
        
        //------Scale------//
        if (Input.touchCount == 2)
        {
            var touch0 = Input.GetTouch(0);
            var touch1 = Input.GetTouch(1);
            float dist = (touch0.position - touch1.position).magnitude;
            if (prevTouchCount < 2)
            {
                prevDist = (touch0.position - touch1.position).magnitude;
                prevScale = selectedObj.transform.localScale;
                isScale = true;
            }
            if (isScale)
                selectedObj.transform.localScale = prevScale * (dist / prevDist);
        }
        else
            isScale = false;
        
        prevTouchCount = Input.touchCount;
    }

    // Public UI methods
    public void OnCaptureButton()
    {
        snapShot.CaptureAndSaveToAlbum(Screen.width, Screen.height, Camera.main, ImageType.JPG);
    }

    public void OnPlaceMachine(int index)
    {
        if (!isDetected)
            return;

        GameObject obj = GameObject.Instantiate(m_machines[index]);
        obj.transform.SetParent(m_machineParentTransform);
        obj.transform.localScale = Vector3.one;
        obj.transform.position = m_targetTransform.position;
        if (selectedObj != null)
            selectedObj.GetComponent<KioskItem>().SelectKiosk(false);
        selectedObj = obj;
        selectedObj.GetComponent<KioskItem>().SelectKiosk(true);
    }

}
