﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KioskItem : MonoBehaviour {
    public GameObject highlight;

    public void SelectKiosk(bool bSelect)
    {
        highlight.SetActive(bSelect);
    }
}
